Source: check-mk-agent
Section: admin
Priority: optional
Maintainer: Juri Grabowski <debian@jugra.de>
Build-Depends:
 debhelper (>= 13),
 dh-exec,
Standards-Version: 4.6.0
Homepage: https://checkmk.de
Vcs-Browser: https://salsa.debian.org/gratuxri/check-mk-agent
Vcs-Git: https://salsa.debian.org/gratuxri/check-mk-agent.git
Rules-Requires-Root: no

Package: check-mk-agent
Architecture: all
Depends:
 time,
 waitmax,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 python3,
 xinetd,
Description: general purpose monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the agent part of check-mk-agent.
 Note that the check-mk-server WATO has not been packaged for debian
 (yet) and has to be downloaded and installed directly from upstream.

Package: waitmax
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Architecture specific waitmax for Check_MK Agent for Linux
 The Checkmk Agent uses xinetd or systemd to provide information about the
 system on TCP port 6556. This can be used to monitor the host via Checkmk.

Package: check-mk-agent-plugin-apache-status
Architecture: all
Depends:
 check-mk-agent,
 python3,
 python3-urllib3,
 ${misc:Depends},
Description: general purpose Apache monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the apache_status plugin for the agent.

Package: check-mk-agent-plugin-asmcmd
Architecture: all
Depends:
 ${misc:Depends},
Description: general purpose asmcmd monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the asmcmd plugin for the agent.

Package: check-mk-agent-plugin-dnsclient
Architecture: all
Depends:
 bind9-dnsutils,
 check-mk-agent,
 ${misc:Depends},
Description: general purpose dns monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the dnsclient plugin for the agent.

Package: check-mk-agent-plugin-isc-dhcpd
Architecture: all
Depends:
 check-mk-agent,
 isc-dhcp-server,
 python3,
 ${misc:Depends},
Description: general purpose isc-dhcpd monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the isc_dhcp plugin for the agent.

Package: check-mk-agent-plugin-jar-signature
Architecture: all
Depends:
 check-mk-agent,
 default-jre-headless,
 ${misc:Depends},
Description: general purpose jar-signature monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the jar_signature plugin for the agent.

Package: check-mk-agent-plugin-kaspersky-av
Architecture: all
Depends:
 check-mk-agent,
 ${misc:Depends},
Description: general purpose kaspersky-av monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the kaspersky_av plugin for the agent.

Package: check-mk-agent-plugin-lnx-quota
Architecture: all
Depends:
 check-mk-agent,
 quota,
 ${misc:Depends},
Description: general purpose quota monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the lnx_quota plugin for the agent.

Package: check-mk-agent-plugin-lvm
Architecture: all
Depends:
 check-mk-agent,
 lvm2,
 ${misc:Depends},
Description: general purpose lvm monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the lvm plugin for the agent.

Package: check-mk-agent-plugin-apt-hourly
Architecture: all
Depends:
 apt,
 check-mk-agent,
 ${misc:Depends},
Description: general purpose apt monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the apt plugin for the agent.

Package: check-mk-agent-plugin-ceph
Architecture: all
Depends:
 ceph-common,
 check-mk-agent,
 ${misc:Depends},
Description: general purpose ceph monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the ceph plugin for the agent.

Package: check-mk-agent-plugin-cups-queues
Architecture: all
Depends:
 check-mk-agent,
 cups | cups-client,
 ${misc:Depends},
Description: general purpose CUPS queues monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the cups_queues plugin for the agent.

Package: check-mk-agent-plugin-docker
Architecture: all
Depends:
 check-mk-agent,
 docker.io,
 python3,
 python3-multiprocess,
 ${misc:Depends},
Description: general purpose docker monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the docker plugin for the agent.

Package: check-mk-agent-plugin-filehandler
Architecture: all
Depends:
 check-mk-agent,
 ${misc:Depends},
Description: general purpose filehandler monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the filehandler plugin for the agent.

Package: check-mk-agent-plugin-filestats
Architecture: all
Depends:
 check-mk-agent,
 python3,
 ${misc:Depends},
Description: general purpose filestats monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the filestats plugin for the agent.

Package: check-mk-agent-plugin-inotify
Architecture: all
Depends:
 check-mk-agent,
 python3,
 python3-cysignals-bare,
 ${misc:Depends},
Description: general purpose inotify monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the mk_inotify plugin for the agent.

Package: check-mk-agent-plugin-inventory
Architecture: all
Depends:
 check-mk-agent,
 dmidecode,
 pciutils,
 procps,
 ${misc:Depends},
Description: general purpose inventory monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the inventory plugin for the agent.

Package: check-mk-agent-plugin-iptables
Architecture: all
Depends:
 check-mk-agent,
 iptables,
 ${misc:Depends},
Description: general purpose iptables monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the iptables plugin for the agent.

Package: check-mk-agent-plugin-jolokia
Architecture: all
Depends:
 check-mk-agent,
 python3,
 ${misc:Depends},
Description: general purpose jolokia monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the jolokia plugin for the agent.

Package: check-mk-agent-plugin-logins
Architecture: all
Depends:
 ${misc:Depends},
Description: general purpose logins monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the logins plugin for the agent.

Package: check-mk-agent-plugin-logwatch
Architecture: all
Depends:
 check-mk-agent,
 python3,
 python3-asteval,
 ${misc:Depends},
Description: general purpose logwatch monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the logwatch plugin for the agent.

Package: check-mk-agent-plugin-mongodb
Architecture: all
Depends:
 check-mk-agent,
 python3,
 python3-pymongo,
 ${misc:Depends},
Description: general purpose mongodb monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the mongodb plugin for the agent.

Package: check-mk-agent-plugin-mysql
Architecture: all
Depends:
 check-mk-agent,
 default-mysql-client,
 ${misc:Depends},
Description: general purpose MySQL monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the MySQL plugin for the agent.

Package: check-mk-agent-plugin-nfsiostat
Architecture: all
Depends:
 check-mk-agent,
 nfs-common,
 ${misc:Depends},
Description: general purpose NFS monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the nfsiostat plugin for the agent.

Package: check-mk-agent-plugin-omreport
Architecture: all
Depends:
 check-mk-agent,
 srvadmin-base | srvadmin-all,
 ${misc:Depends},
Description: general purpose DELL monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the omreport plugin for the agent.

Package: check-mk-agent-plugin-postgres
Architecture: all
Depends:
 check-mk-agent,
 python3,
 python3-sh,
 ${misc:Depends},
Description: general purpose postgres monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the postgres plugin for the agent.

Package: check-mk-agent-plugin-scaleio
Architecture: all
Depends:
 check-mk-agent,
 emc-scaleio-sdc,
 ${misc:Depends},
Description: general purpose scaleio monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the scaleio plugin for the agent.

Package: check-mk-agent-plugin-site-object-counts
Architecture: all
Depends:
 check-mk-agent,
 check-mk-raw,
 ucspi-unix,
 ${misc:Depends},
Description: general purpose omd monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the site_object_counts plugin for the agent.

Package: check-mk-agent-plugin-sshd-config
Architecture: all
Depends:
 check-mk-agent,
 openssh-server,
 ${misc:Depends},
Description: general purpose sshd_config monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the sshd_config plugin for the agent.

Package: check-mk-agent-plugin-tinkerforge
Architecture: all
Depends:
 check-mk-agent,
 python3,
 python3-urllib3,
 ${misc:Depends},
Description: general purpose tinkerforge monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the tinkerforge plugin for the agent.

Package: check-mk-agent-plugin-mtr
Architecture: all
Depends:
 check-mk-agent,
 mtr-tiny | mtr,
 python3,
 ${misc:Depends},
Description: general purpose mtr monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the mtr plugin for the agent.

Package: check-mk-agent-plugin-netstat
Architecture: all
Depends:
 check-mk-agent,
 net-tools,
 ${misc:Depends},
Description: general purpose netstat monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the netstat plugin for the agent.

Package: check-mk-agent-plugin-nfsexports
Architecture: all
Depends:
 check-mk-agent,
 nfs-common,
 nfs-server,
 ${misc:Depends},
Description: general purpose nfsexports monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the nfsexports plugin for the agent.

Package: check-mk-agent-plugin-nginx-status
Architecture: all
Depends:
 check-mk-agent,
 nginx | nginx-full,
 python3,
 python3-urllib3,
 ${misc:Depends},
Description: general purpose nginx monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the nginx_status plugin for the agent.

Package: check-mk-agent-plugin-runas
Architecture: all
Depends:
 check-mk-agent,
 ${misc:Depends},
Description: This plugin allows one to run check-mk-agent as different user
 general purpose monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the runas plugin for the agent.
 This plugin allows one to execute mrpe, local and plugin skripts with a
 different user context

Package: check-mk-agent-plugin-smart-hourly
Architecture: all
Depends:
 check-mk-agent,
 smartmontools,
 ${misc:Depends},
Recommends:
 megacli | tw-cli,
Description: general purpose smart monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the smart plugin for the agent.

Package: check-mk-agent-plugin-symantec-av
Architecture: all
Depends:
 check-mk-agent,
 sav,
 ${misc:Depends},
Description: general purpose symantec AV monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the symantec_av plugin for the agent.

Package: check-mk-agent-plugin-redis
Architecture: all
Depends:
 check-mk-agent,
 redis-tools,
 ${misc:Depends},
Description: general purpose redis monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the redis plugin for the agent.

Package: check-mk-agent-plugin-zorp
Architecture: all
Depends:
 check-mk-agent,
 zorp,
 ${misc:Depends},
Description: general purpose zorp monitoring plugin for retrieving data
 Check_mk adopts a new a approach for collecting data from operating systems
 and network components. It obsoletes NRPE, check_by_ssh, NSClient and
 check_snmp. It has many benefits, the most important of which are:
 .
  * Significant reduction of CPU usage on the monitoring host.
  * Automatic inventory of items to be checked on hosts.
 .
 This package contains the zorp plugin for the agent.
