#!/bin/bash
shopt -s extglob
git clean -fxd
git rm -rf .!(debian|agents|COPYING|AUTHORS|defines.make|.git)
git rm -rf agents/wnx\
        agents/z_os\
        agents/windows\
        agents/special\
        agents/sap\
        agents/modules\
        agents/check_mk_agent.hpux\
        agents/check_mk_agent.openbsd\
        agents/check-mk-agent.spec\
        agents/check_mk_agent.openwrt\
        agents/check_mk_agent.openvms\
        agents/cfg_examples/sqlnet.ora\
        agents/cfg_examples/mk_oracle.cfg\
        agents/cfg_examples/tnsnames.ora\
        agents/cfg_examples/saprouter.cfg\
        agents/cfg_examples/sqlplus.sh\
        agents/plugins/mk_inventory.solaris\
        agents/plugins/mk_suseconnect\
        agents/plugins/db2_mem\
        agents/plugins/netstat.aix\
        agents/plugins/mk_saprouter\
        agents/plugins/.f12\
        agents/plugins/unitrends_backup\
        agents/plugins/mk_zypper\
        agents/plugins/mk_sap_hana\
        agents/plugins/mk_oracle_crs\
        agents/plugins/unitrends_replication.py\
        agents/plugins/hpux_statgrab\
        agents/plugins/mk_haproxy.freebsd\
        agents/plugins/.gitignore\
        agents/plugins/mk_oracle\
        agents/plugins/nfsexports.solaris\
        agents/plugins/vxvm\
        agents/plugins/netstat.solaris\
        agents/plugins/mk_db2.aix\
        agents/plugins/mk_db2.linux\
        agents/plugins/Makefile\
        agents/plugins/mk_inventory.aix\
        agents/plugins/mk_errpt.aix\
        agents/plugins/mk_sap.py\
        agents/plugins/websphere_mq\
        agents/plugins/mk_informix\
        agents/plugins/mk_sap.aix\
        agents/plugins/hpux_lunstats\
        agents/CONTENTS\
        agents/mk-job.aix\
        agents/mk-job.solaris\
        agents/.gitignore\
        agents/check_mk_agent.aix\
        agents/check_mk_agent.freebsd\
        agents/.f12\
        agents/waitmax\
        agents/check_mk_agent.macosx\
        agents/check_mk_agent.netbsd\
        agents/check_mk_agent.solaris\
        agents/__init__.py\


git commit -a -m 'clean'
exit 0
